const { Router } = require('express');
const Bikes = require('../models/bikes-model');
const { uuid } = require('uuidv4');
const router = Router();
const { check, validationResult } = require('express-validator')

router.get('/', async (req, res) => {
    try {
        const bikes = await Bikes.find({ rented: false });
        res.status(200).json(bikes);
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
})
router.post('/create', [
        check('name', 'Bike`s name is too short').exists().isLength({ min: 3 }),
        check('price', 'Price is not a Number').exists().isNumeric()

], async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
              errors: errors.array(),
              message: 'Incorrect data'
            })
          }

        const newBike = new Bikes({
            id: uuid(),
            name: req.body.name,
            type: req.body.type,
            price: req.body.price,
            rented: false
        });
        await newBike.save();
        res.status(201).json(newBike);
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
})

router.put('/discount/:id', async (req, res) => {
    try {
        const bike = await Bikes.findOne({ id: req.body.id })
        await Bikes.updateOne({
            id: req.body.id,
            rented: true
        }, {
            price: bike.discount,
            date: null
        },
            { upsert: true });
        res.status(200).json({ message: 'discount used' })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
})
router.delete('/delete/:id', async (req, res) => {
    try {
        await Bikes.deleteOne({
            id: req.body.id,
            rented: false
        })
        res.status(200).json({ message: 'bike deleted' })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }

})

router.put('/remove/:id', async (req, res) => {
    try {
        const bike = await Bikes.findOne({ id: req.body.id })
        await Bikes.updateOne({
            id: req.body.id,
            rented: true
        }, {
            rented: false,
            date: null,
            price: bike.discount * 2
        },
            { upsert: true });

        res.status(200).json({ message: 'rent canceled' })
    } catch (e) {
        res.status(500).json({ message: e.message });
    }
})


router.get('/rented', async (req, res) => {
    try {
        const data = await Bikes.find({ rented: true })
        res.status(200).json(data);
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
})


router.put('/rent/:id', async (req, res) => {
    try {
        const bike = await Bikes.findOne({ id: req.body.id })
        await Bikes.updateOne({
            id: req.body.id,
            rented: false
        },
            {
                rented: true,
                date: new Date(),
                discount: bike.price / 2
            },
            { upsert: true })


        res.status(200).json({ message: 'bike rented' })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
})

module.exports = router;




