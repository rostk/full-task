const { Schema, model } = require('mongoose');

const rentalBikes = new Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    rented: {
        type: Boolean,
        required: true
    },
    date : {
        type : Date,
        default : null
    },
    discount: Number


})

module.exports = model('Bikes', rentalBikes);