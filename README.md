    TechStack F U L L S T A C K  T E S T  T A S K

To clone repository

### git clone https://rostk@bitbucket.org/rostk/full-task.git


--To run server and client together

### `npm run dev`

Server runs on port 5000 [http://localhost:5000](http://localhost:5000)
Client runs on port 3000[http://localhost:3000](http://localhost:3000)


--To start the server

### `npm run server`

Server runs on port 5000 [http://localhost:5000](http://localhost:5000)


--To run client app
### `cd client`
### `npm run start`

Client runs on port 3000[http://localhost:3000](http://localhost:3000)





